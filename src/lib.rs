//! Terminus manages owned values which may need to be available or
//! cleaned up in multiple contexts.
//!
//! ```
//! use once_cell::sync::Lazy;
//! use terminus::{Registry, EntryId, create_registry, clear_registry, register, unregister};
//! use signal_hook::{iterator::Signals, SIGINT, SIGTERM};
//! use std::{fs::File, process, thread};
//!
//! create_registry!(FILE_REGISTRY, std::fs::File);
//!
//! const TERMINATION_SIGNALS: &[i32] = &[SIGINT, SIGTERM];
//!
//! static SIGNAL_HANDLER_THREAD: Lazy<()> = Lazy::new(|| {
//!     thread::spawn(move || {
//!         for sig in Signals::new(TERMINATION_SIGNALS)
//!             .expect("failed to initialize signal handlers")
//!             .forever()
//!         {
//!             clear_registry!(FILE_REGISTRY);
//!             process::exit(sig);
//!         }
//!     });
//! });
//!
//! fn register_file(path: &str) -> EntryId {
//!     Lazy::force(&SIGNAL_HANDLER_THREAD);
//!     let file = std::fs::File::open(path).unwrap();
//!
//!     match register!(FILE_REGISTRY, file) {
//!         Some(entry_id) => entry_id,
//!         None => panic!("Failed to add file to registry"),
//!     }
//! }
//!
//! fn unregister_file(id: &EntryId) {
//!     Lazy::force(&SIGNAL_HANDLER_THREAD);
//!     match unregister!(FILE_REGISTRY, id) {
//!         Some(_file) => println!("Successfully unregistered file corresponding to {:?}", id),
//!         None => panic!("Entry corresponding to {:?} has already been dropped", id),
//!     }
//! }
//!
//! fn drop_all_file_references() {
//!     Lazy::force(&SIGNAL_HANDLER_THREAD);
//!     clear_registry!(FILE_REGISTRY);
//! }
//! ```

use std::{collections::HashMap, fmt};
use tracing::trace;

/// Identifies entries in a `Registry`.
#[derive(Clone, Copy, Debug, Default, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct EntryId(usize);

impl EntryId {
    /// Generate the next `EntryId`.
    pub fn next(&self) -> Self {
        Self(self.0 + 1)
    }
}

/// Owned values of some type, `T`, each indexed by `EntryId`. Useful
/// for tracking potentially long-lived values which must be dropped
/// in multiple contexts.
///
/// The `create_registry`, `register`, `unregister`, and
/// `clear_registry` macros manage a static `Registry`.
pub struct Registry<T> {
    entry_id: EntryId,
    entries: HashMap<EntryId, T>,
}

impl<T> Default for Registry<T> {
    fn default() -> Self {
        Self {
            entry_id: Default::default(),
            entries: Default::default(),
        }
    }
}

impl<T> fmt::Debug for Registry<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Registry")
            .field("entry_id", &self.entry_id)
            .field(
                "entries",
                &format!("map with {} entries", self.entries.len()),
            )
            .finish()
    }
}

impl<T> Registry<T> {
    /// Create a new `Registry` with zero entries.
    #[tracing::instrument]
    pub fn new() -> Self {
        Default::default()
    }

    /// Register owned value, `item`, and return a corresponding
    /// `EntryId` when insertion succeeds.
    #[tracing::instrument(skip(item))]
    pub fn register(&mut self, item: T) -> Option<EntryId> {
        let id = self.next_id();

        trace!(?id, "registering item");
        if self.insert_with_id(id, item) {
            Some(id)
        } else {
            None
        }
    }

    /// Unregister entry corresponding to `id` and return if it exists.
    #[tracing::instrument]
    pub fn unregister(&mut self, id: &EntryId) -> Option<T> {
        trace!(?id, "Unregistering entry");
        self.entries.remove(id)
    }

    /// Clear all entries from the registry. Entries are owned values,
    /// so `std::mem::drop` gets called on each of them.
    #[tracing::instrument]
    pub fn clear(&mut self) {
        trace!(entry_count = self.len(), "Clearing entries from registry");
        self.entries.clear();
        self.entry_id = Default::default();
        debug_assert!(self.is_empty());
    }

    /// Number of entries in the registry.
    #[tracing::instrument]
    pub fn len(&self) -> usize {
        trace!("Checking number of entries in the registry");
        self.entries.len()
    }

    /// True if and only if there are zero entries in the registry.
    #[tracing::instrument]
    pub fn is_empty(&self) -> bool {
        trace!("Checking number of entries in the registry");
        self.entries.is_empty()
    }

    fn next_id(&mut self) -> EntryId {
        trace!("finding next available entry ID");
        while self.entries.contains_key(&self.entry_id) {
            self.entry_id = self.entry_id.next();
        }
        self.entry_id
    }

    fn insert_with_id(&mut self, id: EntryId, item: T) -> bool {
        use std::collections::hash_map::Entry::*;

        match self.entries.entry(id) {
            Occupied(_) => false,
            Vacant(slot) => {
                slot.insert(item);
                true
            }
        }
    }
}

/// Create a static `Registry` identified by the first argument that
/// contains values of the specified type (second argument).
///
/// ```
/// use terminus::*;
///
/// create_registry!(FILE_REGISTRY, std::fs::File);
/// ```
#[macro_export]
#[rustfmt::skip]
macro_rules! create_registry {
    ($static_id:ident, $type:ty) => {
        paste::paste! {
            const [<$static_id _POISON_ERROR>]: &str = "read-write lock is poisoned";

            // static [<$static_id>]: std::sync::RwLock<Registry<$type>> =
            //     std::sync::RwLock::new(Registry::new());

            static [<$static_id>]: once_cell::sync::Lazy<std::sync::RwLock<Registry<$type>>> = once_cell::sync::Lazy::new(|| {
                std::sync::RwLock::new(Registry::new())
            });
        }
    };
}

/// Register a value of the appropriate type (second argument) to the
/// identified static `Registry` (first argument).
///
/// ```
/// use terminus::*;
///
/// create_registry!(FILE_REGISTRY, std::fs::File);
///
/// fn register_file(path: &str) -> EntryId {
///     let handle = std::fs::File::open(path).unwrap();
///     register!(FILE_REGISTRY, handle).unwrap()
/// }
/// ```
#[macro_export]
#[rustfmt::skip]
macro_rules! register {
    ($static_id:ident, $item:expr) => {
        {
            paste::paste! {
                let mut reg = [<$static_id>].write().expect([<$static_id _POISON_ERROR>]);
                tracing::trace!(entry_count = reg.len(), "Registering entry to {}", stringify!([<$static_id>]));
                reg.register($item)
            }
        }
    };
}

/// Unregister the entry corresponding to the specified `EntryId`
/// (second argument) from the identified static `Registry` (first
/// argument).
///
/// ```
/// use terminus::*;
///
/// create_registry!(FILE_REGISTRY, std::fs::File);
///
/// fn register_file(path: &str) -> EntryId {
///     let file = std::fs::File::open(path).unwrap();
///
///     match register!(FILE_REGISTRY, file) {
///         Some(entry_id) => entry_id,
///         None => panic!("Failed to add file to registry"),
///     }
/// }
///
/// fn unregister_file(id: &EntryId) {
///     match unregister!(FILE_REGISTRY, id) {
///         Some(_file) => println!("Successfully unregistered file corresponding to {:?}", id),
///         None => panic!("Entry corresponding to {:?} has already been dropped", id),
///     }
/// }
/// ```
#[macro_export]
#[rustfmt::skip]
macro_rules! unregister {
    ($static_id:ident, $entry_id:expr) => {
        {
            paste::paste! {
                let mut reg = [<$static_id>]
                    .write()
                    .expect([<$static_id _POISON_ERROR>]);
                reg.unregister($entry_id)
            }
        }
    };
}

/// Clear out the specified static `Registry`.
///
/// ```
/// use terminus::*;
///
/// create_registry!(FILE_REGISTRY, std::fs::File);
///
/// fn terminate() {
///     clear_registry!(FILE_REGISTRY);
/// }
/// ```
#[macro_export]
#[rustfmt::skip]
macro_rules! clear_registry {
    ($static_id:ident) => {
        {
            paste::paste! {
                let mut reg = [<$static_id>].write().expect([<$static_id _POISON_ERROR>]);
                tracing::trace!(entry_count = reg.len(), "Clearing {}", stringify!([<$static_id>]));
                reg.clear();
            }
        }
    };
}

#[cfg(test)]
#[macro_use]
extern crate serial_test;

#[cfg(test)]
mod kill_child_on_drop_tests {
    use super::*;
    use once_cell::sync::Lazy;
    use signal_hook::{iterator::Signals, SIGINT, SIGTERM};
    use std::{process, thread, time::Duration};
    use tracing::info;
    use xand_utils::procutils::KillChildOnDrop;

    // Enable to manually test signal handling logic. Press Ctrl+c
    // when the test sleeps.
    const MANUALLY_TESTING_SIGNAL_HANDLER_CLEANUP: bool = false;

    // For now `TERMINATION_SIGNALS` and `SIGNAL_HANDLER_THREAD` are
    // here primarily for demonstration purposes. To manually test
    // them enable MANUALLY_TESTING_SIGNAL_HANDLER_CLEANUP so there's
    // time to press Ctrl+c while the tests are running, and verify no
    // new 'yes' processes are running.
    const TERMINATION_SIGNALS: &[i32] = &[SIGINT, SIGTERM];

    // Create a global child process registry for KillChildOnDrop
    // values.
    create_registry!(CHILD_PROCESS_REGISTRY, KillChildOnDrop);

    static GLOBAL_SETUP: Lazy<()> = Lazy::new(|| {
        // set up tracing logs
        let subscriber = tracing_subscriber::FmtSubscriber::builder()
            .with_max_level(tracing::Level::TRACE)
            .finish();
        tracing::subscriber::set_global_default(subscriber)
            .expect("setting default tracing subscriber failed");

        thread::spawn(move || {
            info!("Setting up signal handlers ...");

            for sig in Signals::new(TERMINATION_SIGNALS)
                .expect("failed to initialize signal handlers")
                .forever()
            {
                info!(sig, "Received signal");
                clear_registry!(CHILD_PROCESS_REGISTRY);
                assert!(!long_process_is_running());
                process::exit(sig);
            }
        });
    });

    // must be called by each test, even though only the first one to
    // run it forces the lazy value to evaluate
    fn setup() {
        Lazy::force(&GLOBAL_SETUP);
    }

    #[test]
    #[serial]
    fn track_child_processes_static_registry_and_kill_all() {
        setup();
        assert!(!long_process_is_running());

        for _ in 0..10 {
            thread::spawn(|| {
                register!(CHILD_PROCESS_REGISTRY, long_running_process());
            });
            assert!(long_process_is_running());
        }

        if MANUALLY_TESTING_SIGNAL_HANDLER_CLEANUP {
            trace!(
                ?MANUALLY_TESTING_SIGNAL_HANDLER_CLEANUP,
                "sleeping for 10 seconds so there's an opportunity to press Ctrl+c"
            );
            thread::sleep(Duration::from_secs(10));
        } else {
            trace!(
                ?MANUALLY_TESTING_SIGNAL_HANDLER_CLEANUP,
                "manual testing disabled; no need to sleep"
            );
        }

        clear_registry!(CHILD_PROCESS_REGISTRY);
        assert!(!long_process_is_running());
    }

    #[test]
    #[serial]
    fn test_long_process_is_running() {
        setup();
        assert!(!long_process_is_running());

        let dropme = long_running_process();
        assert!(long_process_is_running());
        drop(dropme);

        assert!(!long_process_is_running());
    }

    fn long_running_process() -> KillChildOnDrop {
        let mut cmd = process::Command::new("yes");
        cmd.stdout(process::Stdio::null());

        let mut child = KillChildOnDrop::new(cmd);
        child.spawn().unwrap();

        child
    }

    // WARNING: This returns false positives if any other running
    // processes cause output from `ps aux` to include the text 'yes'.
    fn long_process_is_running() -> bool {
        let output = process::Command::new("ps")
            .arg("aux")
            .output()
            .expect("failed to spawn `ps aux`");

        let output = String::from_utf8_lossy(&output.stdout);

        output.contains("yes")
    }
}
