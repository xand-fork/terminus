# Terminus

Terminus manages owned values which may need to be available or
cleaned up in multiple contexts.

The following demo is extracted from the crate documentation. In case
this gets stale run `cargo doc --open` to see the latest, which is
checked by the compiler.

```rust
use once_cell::sync::Lazy;
use terminus::{Registry, EntryId, create_registry, clear_registry, register, unregister};
use signal_hook::{iterator::Signals, SIGINT, SIGTERM};
use std::{fs::File, process, thread};

create_registry!(FILE_REGISTRY, std::fs::File);

const TERMINATION_SIGNALS: &[i32] = &[SIGINT, SIGTERM];

static SIGNAL_HANDLER_THREAD: Lazy<()> = Lazy::new(|| {
    thread::spawn(move || {
        for sig in Signals::new(TERMINATION_SIGNALS)
            .expect("failed to initialize signal handlers")
            .forever()
        {
            clear_registry!(FILE_REGISTRY);
            process::exit(sig);
        }
    });
});

fn setup() {
    Lazy::force(&SIGNAL_HANDLER_THREAD);
}

fn register_file(path: &str) -> EntryId {
    setup();
    let file = std::fs::File::open(path).unwrap();

    match register!(FILE_REGISTRY, file) {
        Some(entry_id) => entry_id,
        None => panic!("Failed to add file to registry"),
    }
}

fn unregister_file(id: &EntryId) {
    setup();
    match unregister!(FILE_REGISTRY, id) {
        Some(_file) => println!("Successfully unregistered file corresponding to {:?}", id),
        None => panic!("Entry corresponding to {:?} has already been dropped", id),
    }
}

fn drop_all_file_references() {
    setup();
    clear_registry!(FILE_REGISTRY);
}
```
